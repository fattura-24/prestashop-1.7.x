{*
* Questo file è parte del plugin Prestashop v1.7.x di Fattura24
* Autore: Fattura24.com <info@fattura24.com> 
*
* Descrizione: template per visualizzare i campi codice univoco e pec nel cliente lato admin
*}
<div class="col">
    <div class="card">
        <div class="card-header"><i class="material-icons">person</i> {$section['label']|escape:'htmlall':'UTF-8'}{l s='Dati Fattura Elettronica' mod='fattura24'}</div>
        <form id="customer_note" class="form-horizontal" action="{$linkController}" method="post" >
            <div class="card-body">
                <div class="form-wrapper">
                    <input type="hidden" name="id_fattura24" value="{$id_fattura24}"></input>
                    <span class="fattura24_codice_destinatario">{l s='Codice Destinatario' mod='fattura24'} <input type="text" maxlength="7" name="fattura24_codice_destinatario" value="{$fattura24_codice_destinatario}" readonly></span><br />
                    <span class="fattura24_pec">{l s='Indirizzo PEC' mod='fattura24'} <input type="text" name="fattura24_pec" value="{$fattura24_pec}" readonly></span>
                
                    <button type="button" onclick="input_readOnly(this)">{l s='Modifica' mod='fattura24'}</button>                     
                </div>
            <div class="col-lg-12">
                <button type="submit" id="submitPecSdicode" class="btn-default pull-right" disabled="disabled">
                    <i class="icon-save"></i>
                    {l s='Save' mod='fattura24'}
                </button>
            </div>
        </div>
        </form>
    </div>    
</div>            
   
<script>
    function input_readOnly(obj){
        var PecSdicodeForm = obj.form;
        PecSdicodeForm['fattura24_codice_destinatario'].readOnly = false;
        PecSdicodeForm['fattura24_pec'].readOnly = false;
        document.getElementById("submitPecSdicode").disabled = false;  
    }
</script>