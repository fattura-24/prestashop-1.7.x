# [Plugin Fattura24 per Prestashop](http://www.fattura24.com/prestashop-modulo-fatturazione/)

Prestashop è diventato in pochissimo tempo il CMS di commercio elettronico più diffuso in Italia.
Completamente gratuito, è lo strumento ideale per chiunque voglia aprire velocemente un e-commerce professionale senza avere particolari conoscenze tecniche.
In questo articolo vedremo come estendere le funzionalità di fatturazione di Prestashop integrandolo con il servizio di fatturazione di [Fattura24](http://www.fattura24.com/).

### step 1 – preparazione
Per utilizzare Fattura24 come servizio di fatturazione in Prestashop ti occorre:
- un abbonamento Business attivo su Fattura24
- l’API KEY associata al tuo abbonamento Business
per ottenerla vai in ‘Configurazione’->’App e Servizi esterni’, clicca sul pulsante ‘Configura’ di Prestashop e richiedi l’API KEY che ti sarà subito mostrata
- scaricare il modulo di Fattura24 per Prestashop

### step 2 – configurazione
Di seguito i passaggi da seguire all’interno della console di amministrazione di Prestashop
- da menu vai in ‘Moduli’ -> ’Moduli e servizi’
- clicca sul pulsante ‘Aggiungi nuovo modulo’
- seleziona e carica il file zip scaricato in questa pagina
- clicca sul pulsante ‘Carica il modulo’
- nella sezione ‘Elenco moduli’ cercare il modulo ‘Fattura24’
- clicca sul pulsante verde ‘Installa’ e quando ti verrà richiesto clicca su ‘Procedi con l’installazione’
- inserisci nel campo ‘Api Key’ la tua API KEY di Fattura24
- clicca su ‘Verifica Api Key’ per accertarti di aver inserito l’API KEY correttamente
- spunta i campi che ti occorrono
- salva
- assicurati che in ‘Parametri avanzati’ -> ‘Prestazioni’ siano poste su ‘No’ le seguenti impostazioni:
    - ‘Disattiva moduli non nativi di PrestaShop’
    - ‘Disattiva tutti gli override’
Può capitare che quando si aggiorna la propria versione di Prestashop questi campi vengano automaticamente posti su ‘Si’. Assicurati di reimpostarli su ‘No’.
- da menu vai in ‘Ordini’ -> ’Stati’ e per quanto riguarda lo stato ‘Pagamento accettato’ accertati che sia spuntata la voce ‘Segna Ordine come pagato’. Il modulo in automatico spunta la voce ‘Consente a un cliente di scaricare e guardare le versioni PDF delle sue fatture’ che permette al cliente di scaricare il PDF della fattura dalla pagina dell’ordine.

### step 3 – verifica
La configurazione si può considerare terminata e possiamo procedere con un test.
Ogni qual volta verrà creato un ordine sul tuo e-commerce il modulo che hai installato farà per conto tuo le seguenti azioni:
- se hai spuntato ‘Salva cliente’ nelle impostazioni, aggiungerà il cliente nella rubrica di Fattura24 o ne aggiornerà i dati se già presente
- se hai spuntato ‘Crea ordine’, invierà a Fattura24 i dati dell’ordine
- se hai spuntato ‘Invia email’, verrà inviata un’email al cliente con il PDF dell’ordine
Ogni qual volta modificherai un ordine ricevuto, impostando il suo stato in ‘Pagamento accettato’, il modulo eseguirà le seguenti azioni:
- se hai spuntato ‘Crea fattura’ nelle impostazioni, creerà in Fattura24 una ricevuta se il cliente non ha una Partita IVA, una fattura altrimenti
- aggiungerà il cliente nella tua rubrica o ne aggiornerà i dati se già presente
- creerà il PDF della ricevuta/fattura
- se hai spuntato ‘Scarica PDF’ scaricherà una copia del PDF dentro il tuo e-commerce
- se hai spuntato ‘Invia email’, verrà inviata un’email al cliente con il PDF della ricevuta/fattura
- se hai spuntato ‘Stato pagata’, la ricevuta/fattura sarà creata in Fattura24 direttamente nello stato ‘pagata’
- se hai spuntato ‘Disabilita ricevute’, verrà creata una fattura anziché una ricevuta anche in assenza della Partita IVA

Invece dello stato ‘Pagamento accettato’ puoi anche usare degli stati personalizzati per eseguire automaticamente questi passaggi, assicurandoti di spuntare nella configurazione dello stato personalizzato la voce ‘Segna Ordine come pagato’ affinché lo stato attivi la creazione della ricevuta/fattura. Spunta anche ‘Consente a un cliente di scaricare e guardare le versioni PDF delle sue fatture’ se vuoi permettere al cliente di scaricare il PDF della fattura dalla pagina dell’ordine.

Come hai visto utilizzare Fattura24 dentro Prestashop è semplicissimo ma se volessi assistenza tecnica non esitare a contattarci al numero +39 06-40402261.
Il nostro team tecnico è a tua completa disposizione per permetterti di ottenere il meglio dal tuo e-commerce e da Fattura24.