# Changelog
## 1.8.11
###### _Lug 6, 2020_
- Aggiunti dati delle modalità di pagamento anche nell'ordine
## 1.8.10
###### _Giu 16, 2020_
- Aggiunto messaggio di errore in caso di FE e mancata configurazione della Natura per aliquote 0%
## 1.8.9
###### _Mag 18, 2020_
- Miglioramento prestazioni del plugin
- Aggiunta opzione per la creazione di ricevute
- sostituito il link di accesso a Fattura24 con v3
## 1.8.8
###### _Apr 22, 2020_
- Aggiunta gestione del bollo virtuale per le FE
- Aggiunti metodi di pagamento elettronico
## 1.8.7
###### _Mar 27, 2020_
- Aggiunto il riferimento al numero della fattura (se esistente) nella colonna 'Stato fattura'
## 1.8.6
###### _Mar 11, 2020_
- Modificata la visualizzazione dello stato degli ordini/fatture in F24
- Conversione del codice fiscale in maiuscolo
- Rinomina del file trace.log al momento del download
## 1.8.3
###### _Feb 24, 2020_
- Corretto bug che non consentiva la modifica dei campi PEC e Codice Destinatario lato admin
- migliorato il template di visualizzazione dei campi 
## 1.8.2
###### _Feb 19, 2020_
- Revisione generale del codice
- Aggiunte sezioni CData ai tag xml Object e FootNotes
## 1.8.1
###### _Feb 05, 2020_
- Correzione bug: aggiunta descrizione aliquota IVA per costi di spedizione e coupon
- Rimozione tag xml duplicato
## 1.8.0
###### _Gen 27, 2020_
- Migliorata la gestione dei metodi di pagamento: il tag FePaymentCode viene passato solo per le fatture
- Correzione bug minori
## 1.7.9
###### _Gen 20, 2020_
- Cambiato il metodo di gestione dei pagamenti elettronici
## 1.7.8
###### _Gen 15, 2020_
- corretto bug nel checkout dei pagamenti elettronici con fattura tradizionale
## 1.7.7
###### _Dic 20, 2019_
- cambiata la casella 'Stato Pagato' in un menu a tendina
## 1.7.6
###### _Dic 11, 2019_
- Creazione di fattura in stato 'Pagato' automatica con metodi di pagamento come Paypal
## 1.7.53c
###### _Nov 22, 2019_
- Aggiunta la gestione del piano dei conti anche nell'ordine
## 1.7.52
###### _Nov 7, 2019_
- Aggiunta la possibilità di indicare il riferimento Prestashop nella causale del documento
## 1.7.51
###### _Ott 28, 2019_
- Migliorato il comportamento della pagina di impostazioni
- Cambiata la gestione del campo partita iva per i clienti esteri
## 1.7.50
###### _Ott 23, 2019_
- Cambiata gestione dei coupon: ora l'importo totale viene diviso per aliquota
## 1.7.49
###### _Ott 15, 2019_
- Aggiunta colonna di visualizzazione dello stato dei documenti nel gestionale F24
- Aggiunta gestione della Natura IVA per aliquote di spedizione pari a 0%
- Modificata visualizzazione delle opzioni Natura Iva: ora sono visibili solo se è selezionata l'opzione Fattura Elettronica 
## 1.7.48
###### _Set 24, 2019_
- Migliorata gestione dati pagamento e Iban
- Cambiata la visualizzazione dell'elenco sezionali: ora viene visualizzata l'anteprima
## 1.7.47
###### _Lug 30, 2019_
- Aggiunto pulsante oggetto predefinito
## 1.7.46
###### _Lug 25, 2019_
- Aggiunta gestione pagamenti con Stripe
## 1.7.45
###### _Lug 01, 2019_
- Aggiunto link novità AdE in fondo alla documentazione
## 1.7.44
###### _Mag 30, 2019_
- Aggiunta personalizzazione della causale della fattura
## 1.7.43
###### _Mag 20, 2019_
- Modifica aspetto pagina impostazioni
- Modifica aspetto risposta verifica API Key
## 1.7.42
###### _Mag 11, 2019_
- Aggiunta la data di scadenza come risposta alla verifica API Key
## 1.7.41
###### _Mag 08, 2019_
- Gestione campi PEC e Codice Destinatario lato admin
## 1.7.40
###### _Apr 16, 2019_
- Modifica aspetto pagina impostazioni
## 1.7.39
###### _Apr 08, 2019_
- Modifica etichette riferimenti normativi
## 1.7.38
###### _Apr 04, 2019_
- Aggiunto pulsante per accedere al gestionale Fattura24 dalle impostazioni
## 1.7.37
###### _Apr 02, 2019_
- Ripristino pulsante Verifica API Key
## 1.7.36
###### _Mar 27, 2019_
- Aggiunti riferimenti normativi in calce alle impostazioni
## 1.7.35
###### _Mar 22, 2019_
- Corretto bug su Natura IVA: veniva riportata anche su aliquota standard
- Corretto bug che impediva creazione ordini con Fattura disabilitata
## 1.7.34
###### _Mar 16, 2019_
- Corretto bug su indirizzo di spedizione: il sistema riportava la provincia dall'indirizzo di fatturazione
## 1.7.33
###### _Mar 11, 2019_
- Cambiata gestione campi PEC e Codice Destinatario
## 1.7.32
###### _Feb 13, 2019_
- Corretta gestione dati bancari e Iban
## 1.7.31
###### _Gen 30, 2019_
- Disabilitati ordini a zero
- salvataggio di entrambi i campi pec e codice destinatario (se non vuoti)
- aggiunta nomenclatura per modalità pagamento paypal, assegno
- corretto bug su product_reference
- modifica etichetta riportata in fattura
## 1.7.29
###### _Dic 19, 2018_
- Corretto ulteriore bug nel front office sul download dei pdf, dopo l'aggiornamento per la fatturazione elettronica
# Changelog
## 1.7.28
###### _Dic 14, 2018_
- Corretto bug sul download dei pdf delle fatture, dopo l'aggiornamento per la fatturazione elettronica
## 1.7.27
###### _Dic 07, 2018_
- attivato supporto del plugin alla fatturazione elettronica (Versione Beta)
## 1.7.26
###### _Ago 30, 2018_
- risolto bug sul download delle fatture, nel caso erano presenti fatture con numero progressivo uguale ma create in un altro anno, l'utente riusciva a scaricare solo e sempre quella dell'anno corrente.

## 1.7.25
###### _Apr 04, 2018_
- risolto bug per cui la data del pagamento della fattura creata dall'e-commerce risultava errata in Fattura24

## 1.7.24
###### _Mar 06, 2018_
- aggiunta sezione di Log nella configurazione del modulo in cui è possibile scaricare il file di log di Fattura24 o cancellarlo dal proprio server
- corretto bug per cui la fattura veniva salvata nella cartella di Prestashop con un nome errato anche se poi il nome al dowmload del documento era corretto

## 1.7.23
###### _Feb 13, 2018_ 
- cambiato il comportamento secondo cui vengono create le fatture in Fattura24: se l'ordine ha già una fattura creata con Prestashop, allora non verrà creata una fattura in Fattura24 quando viene cambiato lo stato dell'ordine in Prestashop

## 1.7.22
###### _Feb 08, 2018_ 
- cambiato il comportamento dei pulsanti di visualizzazione fattura: se la fattura non è stata creata in Fattura24 o se è disabilitata la creazione delle fatture nella configurazione del modulo, allora viene visualizzata la fattura generata da Prestashop invece di mostrare un messaggio di errore
- il campo "Oggetto" della ricevuta/fattura in Fattura24 ora viene valorizzato con il numero dell'ordine in Prestashop

## 1.7.21
###### _Gen 19, 2018_ 
- aggiunto l'invio a Fattura24 del nome della tassa associata al prodotto

## 1.7.20
###### _Nov 16, 2017_ 
- aggiunta selezione sezionale per ricevute e fatture nelle impostazioni del modulo. Ora è possibile scegliere il sezionale dei documenti creati dagli e-commerce, cosicché si possono usare sezionali diversi per diversi e-commerce

## 1.7.19
###### _Nov 10, 2017_
- aggiunta selezione conto nelle impostazioni del modulo. Ora è possibile scegliere il conto economico da associare alle prestazioni/prodotto dei documenti generati dall'e-commerce

## 1.7.18
###### _Ott 16, 2017_
- aggiunta selezione modello PDF per ordini e fatture con destinazione. Ora è possibile scegliere modelli diversi per i documenti a seconda che l'ordine contenga o meno l'indirizzo di spedizione del cliente

## 1.7.17
###### _Ott 13, 2017_
- ora il nome del PDF della fattura scaricato in Prestashop contiene l’id fattura e non l’id ordine

## 1.7.16
###### _Ott 12, 2017_
- risolto bug che causava un errore durante l'installazione se la versione di PHP è minore della 5.5

## 1.7.15
###### _Set 15, 2017_
- aggiunto pieno supporto alla funzione multi-negozio di Prestashop
- risolto bug nella pagina di configurazione del modulo

## 1.7.14
###### _Set 12, 2017_
- aggiunti alcuni messaggi di avviso nella pagina di configurazione
- rimossa checkbox 'Scarica PDF' relativa ai PDF delle fatture nella pagina di configurazione. Ora i PDF vengono sempre scaricati quando viene creata una fattura. Solo se è attivata la checkbox 'Crea fattura' è possibile vedere i PDF delle fatture creati con Fattura24 dalla relativa pagina dell'ordine sia lato cliente che amministratore

## 1.7.13
###### _Ago 03, 2017_
- aggiunta selezione modello nella pagina di configurazione del modulo. Ora è possibile scegliere con quale modello vengono creati i PDF degli ordini e delle fatture

## 1.7.12
###### _Lug 27, 2017_
- aggiunti messaggio di avviso su configurazione errata e gestione magazzino nella pagina di configurazione del modulo. Ora è possibile decidere se gli ordini e/o le fatture movimentano il magazzino in Fattura24

## 1.7.11
###### _Lug 12, 2017_
- risolto bug su iva dei coupon

## 1.7.10
###### _Giu 28, 2017_
- aggiunta funzionalità per cui l'ordine impegna la merce nel magazzino di Fattura24 e la fattura la scarica se il prodotto ha lo stesso codice che ha sull'e-commerce

## 1.7.9
###### _Giu 21, 2017_
- risolto bug minore

## 1.7.8
###### _Giu 19, 2017_
- aggiunto avviso ‘Nuova versione disponibile’ nella pagina di configurazione del modulo

## 1.7.7
###### _Giu 6, 2017_
- abilitato l’invio del numero d’ordine a Fattura24
- risolti bug minori

## 1.7.5
###### _Mag 23, 2017_
- risolti bug minori

## 1.7.4
###### _Mag 15, 2017_
- risolti bug minori

## 1.7.3
###### _Mag 11, 2017_
- risolti bug minori

## 1.7.2
###### _Mag 9, 2017_
- risolto bug che causava la creazione di documenti multipli in Fattura24 se l’ordine veniva messo più volte nello stato ‘pagato’

## 1.7.1
###### _Mag 8, 2017_
- risolto bug su invio Codice Fiscale

## 1.7.0
###### _Mag 5, 2017_
- aggiunta compatibilità con Prestashop 1.7.x
- ora il cliente può scaricare il PDF della fattura dalla pagina degli ordini se questa è stata scaricata da Fattura24 in Prestashop
- abilitato il passaggio dei dati relativi a coupon
- abilitato il passaggio dei dati relativi a prodotti scontati
- risolto un altro bug minore sulla visualizzazione del pdf della fattura all’interno del pannello di amministrazione

## 1.6.1
###### _Apr 19, 2017_
- risolto un bug minore sulla visualizzazione del pdf della fattura all’interno del pannello di amministrazione

## 1.3.7
###### _Apr 10, 2017_
- risolto il problema che causava la generazione di una fattura accompagnatoria in assenza di informazioni sulla destinazione della merce

## 1.3.6
###### _Mar 15, 2017_
- abilitato il passaggio dei dati relativi alla destinazione della merce
- abilitato il passaggio dei dati relativi alla modalità di pagamento
- abilitata la configurazione per disabilitare la creazione delle ricevute
- risolti bug minori

## 1.3.5
###### _Mar 3, 2017_
- risolti bug minori